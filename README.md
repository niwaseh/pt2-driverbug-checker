pt2-driverbug-checker
=====================

## 説明
- PT1/2ドライバのバグがあるかどうか確認できるかもしれないツール。
- オリジナル: https://gist.github.com/akimasa/a2fc1fc098dee1e27ab88fab3ff27d23

## 使い方
```sh
git clone https://bitbucket.org/niwaseh/pt2-driverbug-checker.git
cd pt2-driverbug-checker
sed -i "s/.frequencyno = 76,/.frequencyno = 70,/g" pt2-driverbug-checker.c  # 地上波26ch -> 20chに変更 (必要あれば)
gcc -o pt2-driverbug-checker -pthread pt2-driverbug-checker.c && ./pt2-driverbug-checker
```
