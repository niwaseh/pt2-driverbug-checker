#include <stdlib.h>
#include <stdio.h>

#include <semaphore.h>
#include <pthread.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <sys/ioctl.h>

/***************************************************************************/
/* IOCTL定義                                                               */
/***************************************************************************/
#define		SET_CHANNEL	_IOW(0x8D, 0x01, FREQUENCY)
#define		START_REC	_IO(0x8D, 0x02)
#define		STOP_REC	_IO(0x8D, 0x03)
#define		GET_SIGNAL_STRENGTH	_IOR(0x8D, 0x04, int *)
#define		LNB_ENABLE	_IOW(0x8D, 0x05, int)
#define	LNB_DISABLE	_IO(0x8D, 0x06)
typedef	struct	_frequency{
	int		frequencyno ;			// 周波数テーブル番号
	int		slot ;					// スロット番号／加算する周波数
}FREQUENCY;

struct thread_t {
	pthread_t thread;
	sem_t to_thread;
	sem_t to_main;
};

int chktuner(){
	int tfd = open("/dev/pt1video3", O_RDONLY);
	if(tfd < 0){
		fprintf(stderr,"failed to open /dev/pt1video3\n");
		return -1;
	}
	ioctl(tfd, STOP_REC, 0);

	const FREQUENCY freq = {
		.frequencyno = 76,
		.slot = 0,
	};
	if(ioctl(tfd, SET_CHANNEL, &freq) < 0) {
		fprintf(stderr, "Cannot tune pt1video3 to the specified channel: %d\n",errno);
		return -2;
	}

	close(tfd);

	tfd = open("/dev/pt1video2", O_RDONLY);
	if(tfd < 0){
		fprintf(stderr,"failed to open /dev/pt1video2\n");
		return -1;
	}
	ioctl(tfd, STOP_REC, 0);

	if(ioctl(tfd, SET_CHANNEL, &freq) < 0) {
		fprintf(stderr, "Cannot tune pt1video2 to the specified channel: %d\n",errno);
		return -2;
	}
	close(tfd);
	return 0;
}
void *thread_f(void *arg){
	struct thread_t *thread = (struct thread_t *) arg;
	for(int i=0;i<100;i++){
		sem_post(&thread->to_main);
		sem_wait(&thread->to_thread);
		//usleep(1000*10); //10ms
		int tfd = open("/dev/pt1video2", O_RDONLY);
		if(tfd < 0){
			fprintf(stderr,"failed to open pt1video2:%d",errno);
			return (void *) NULL;
		}

		sem_post(&thread->to_main);
		sem_wait(&thread->to_thread);
		//usleep(1000*10); //10ms
		close(tfd);
	}

    return (void *) NULL;
	
}
int main(void){
	int ret;
	if((ret = chktuner()) < 0){
			fprintf(stderr, "Something wrong.\n");
		if(ret == -1){
			fprintf(stderr, "Maybe Mirakurun or Chinachu is using the tuner.\nStop Mirakurun and Chinachu to perform test.\n");
		} else if (ret == -2){
			fprintf(stderr, "Maybe I am using wrong channel.\nPlease change \".frequencyno\" and run again.\n");
			fprintf(stderr, "Or maybe you are running this command twice.\nExecute following commands AS ROOT to activate tuners.\nrmmod pt1_drv; modprobe pt1_drv\n");
		}
		exit(EXIT_FAILURE);
	}
	struct thread_t thread;
	sem_init(&thread.to_main,0,0);
	sem_init(&thread.to_thread,0,0);
	if(pthread_create(&thread.thread, NULL, thread_f, (void *)&thread) != 0){
		fprintf(stderr, "something wrong creating thread\n");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr, "Starting test.\nWait about 32 seconds.\n");
	for(int i=0;i<100;i++){
		sem_wait(&thread.to_main);
		sem_post(&thread.to_thread);
		int tfd2 = open("/dev/pt1video3", O_RDONLY);
		sem_wait(&thread.to_main);
		sem_post(&thread.to_thread);
		close(tfd2);
		fprintf(stderr, "\r%d/100",i+1);
	}

	pthread_join(thread.thread, NULL);
	sem_destroy(&thread.to_main);
	sem_destroy(&thread.to_thread);

	fprintf(stderr, "\n");
	if(chktuner() < 0){
		fprintf(stderr, "Your driver has bug.\nExecute following commands AS ROOT to activate tuners.\nrmmod pt1_drv; modprobe pt1_drv\n");
		exit(EXIT_FAILURE);
	} else {
		fprintf(stderr, "Your driver is probably safe.\nYou can run this command a few times to ensure your driver is safe.\n./pt2-driverbug-checker\n");
		exit(EXIT_SUCCESS);
	}
	
}
